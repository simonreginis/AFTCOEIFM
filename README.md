# User Manual

Python 3.8 is required to enable the script.

The cnn_model folder (with the neural network model inside) should be placed together with the gui.py and requirements.txt files in one folder.

Then create a virtual environment:
### Windows

`python3 -m venv environment_name`
### macOS/Linux

`virtualenv -p python3 environment_name`

Then activate the virtual environment:
### Windows

`environment_name/Scripts/activate.bat`
### macOS/Linux

`source environment_name/bin/activate`

Install dependencies listed in requirements.txt file:
### Windows, macOS, Linux

`pip install -r requirements.txt`

After installing dependencies launch gui.py script **while remaining in the activated virtual environment**:
### Windows, macOS, Linux

`python3 gui.py`

The program was developed and tested on the Windows operating system. Because of this, there is no guarantee that all features will be available on macOS and Linux.
