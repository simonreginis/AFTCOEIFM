from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QWidget
import sys
import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from sklearn import preprocessing
import multiprocessing
from playsound import playsound
import os

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
from scipy.io import wavfile
import tensorflow as tf
import kmeans1d
import warnings
warnings.filterwarnings("ignore")


class Ui_Form(QWidget):
    audiofile = ("", "")
    timestamp = 0
    duration = 0
    window_time = 30
    audiothread = 0
    model = tf.keras.applications.InceptionV3(
        include_top=True,
        weights=None,
        input_shape=(299, 299, 3),
        classes=9,
        classifier_activation="softmax",
    )

    def setupUi(self):
        Form.setObjectName("Form")
        Form.resize(700, 645)
        Form.setMinimumSize(QtCore.QSize(700, 645))
        Form.setMaximumSize(QtCore.QSize(700, 645))

        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        Form.setPalette(palette)

        self.buttonWczytaj = QtWidgets.QPushButton(Form)
        self.buttonWczytaj.setGeometry(QtCore.QRect(20, 20, 131, 31))
        self.buttonWczytaj.setObjectName("buttonWczytaj")
        self.buttonWczytaj.clicked.connect(self.clickedWczytaj)

        self.buttonWyczysc = QtWidgets.QPushButton(Form)
        self.buttonWyczysc.setGeometry(QtCore.QRect(20, 70, 131, 31))
        self.buttonWyczysc.setObjectName("buttonWyczysc")
        self.buttonWyczysc.clicked.connect(self.clickedWyczysc)

        self.buttonAnalizuj = QtWidgets.QPushButton(Form)
        self.buttonAnalizuj.setGeometry(QtCore.QRect(20, 270, 131, 31))
        self.buttonAnalizuj.setObjectName("buttonAnalizuj")
        self.buttonAnalizuj.clicked.connect(self.clickedAnalizuj)

        self.buttonPlay = QtWidgets.QPushButton(Form)
        self.buttonPlay.setGeometry(QtCore.QRect(30, 350, 51, 51))
        self.buttonPlay.setObjectName("buttonPlay")
        self.buttonPlay.setDisabled(True)
        self.buttonPlay.clicked.connect(self.clickedPlay)

        self.buttonStop = QtWidgets.QPushButton(Form)
        self.buttonStop.setGeometry(QtCore.QRect(90, 350, 51, 51))
        self.buttonStop.setObjectName("buttonStop")
        self.buttonStop.setDisabled(True)
        self.buttonStop.clicked.connect(self.clickedStop)

        # self.inputFragment = QtWidgets.QPlainTextEdit(Form)
        self.inputFragment = QtWidgets.QLineEdit(Form)
        self.inputFragment.setGeometry(QtCore.QRect(20, 190, 91, 31))
        self.inputFragment.setWhatsThis("")
        self.inputFragment.setObjectName("inputFragment")
        self.inputFragment.setMaxLength(5)
        self.inputFragment.setValidator(QIntValidator())

        self.graphicsSpektrogram = QtWidgets.QLabel(Form)
        self.graphicsSpektrogram.setGeometry(QtCore.QRect(170, 240, 505, 385))
        self.graphicsSpektrogram.setObjectName("graphicsSpektrogram")

        self.graphicsWave = QtWidgets.QLabel(Form)
        self.graphicsWave.setGeometry(QtCore.QRect(170, 20, 505, 223))
        self.graphicsWave.setObjectName("graphicsWave")

        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(20, 140, 141, 41))
        self.label.setWordWrap(True)
        self.label.setObjectName("label")

        font = QtGui.QFont()
        font.setPointSize(10)

        self.labelWynikKlasyfikacji = QtWidgets.QLabel(Form)
        self.labelWynikKlasyfikacji.setGeometry(QtCore.QRect(20, 420, 121, 41))
        self.labelWynikKlasyfikacji.setFont(font)
        self.labelWynikKlasyfikacji.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.NoContextMenu)
        self.labelWynikKlasyfikacji.setObjectName("labelWynikKlasyfikacji")

        self.labelWynik1 = QtWidgets.QLabel(Form)
        self.labelWynik1.setGeometry(QtCore.QRect(20, 460, 141, 41))
        self.labelWynik1.setFont(font)
        self.labelWynik1.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.NoContextMenu)
        self.labelWynik1.setObjectName("labelWynik1")
        self.labelWynik1.setVisible(False)

        self.labelWynik2 = QtWidgets.QLabel(Form)
        self.labelWynik2.setGeometry(QtCore.QRect(20, 500, 141, 41))
        self.labelWynik2.setFont(font)
        self.labelWynik2.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.NoContextMenu)
        self.labelWynik2.setObjectName("labelWynik2")
        self.labelWynik2.setVisible(False)

        self.labelWynik3 = QtWidgets.QLabel(Form)
        self.labelWynik3.setGeometry(QtCore.QRect(20, 540, 141, 41))
        self.labelWynik3.setFont(font)
        self.labelWynik3.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.NoContextMenu)
        self.labelWynik3.setObjectName("labelWynik3")
        self.labelWynik3.setVisible(False)

        self.progressBar = QtWidgets.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(20, 310, 131, 23))
        self.progressBar.setTextVisible(False)
        self.progressBar.setObjectName("progressBar")
        self.progressBar.setVisible(False)

        self.groupBox = QtWidgets.QGroupBox(Form)
        self.groupBox.setGeometry(QtCore.QRect(10, 410, 151, 211))
        self.groupBox.setObjectName("groupBox")

        self.labelSekundy = QtWidgets.QLabel(Form)
        self.labelSekundy.setGeometry(QtCore.QRect(120, 190, 47, 31))
        self.labelSekundy.setObjectName("labelSekundy")

        self.labelZakres = QtWidgets.QLabel(Form)
        self.labelZakres.setGeometry(QtCore.QRect(20, 230, 131, 21))
        self.labelZakres.setText("")
        self.labelZakres.setObjectName("labelZakres")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        try:
            self.model.load_weights('cnn_model/InceptionV3.h5')
        except:
            self.errorWindow("Nie wykryto modelu sieci neuronowej.")
            self.progressBar.setVisible(False)
            sys.exit()

        if not os.path.isdir('temp'):
            os.mkdir('temp')

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Klasyfikacja emocji w muzyce filmowej"))
        self.buttonWczytaj.setText(_translate("Form", "Wczytaj"))
        self.buttonWyczysc.setText(_translate("Form", "Wyczyść"))
        self.buttonAnalizuj.setText(_translate("Form", "Analizuj"))
        self.label.setText(_translate("Form", "Początek 30-sekundowego fragmentu:"))
        self.labelWynikKlasyfikacji.setText(_translate("Form", "Wynik klasyfikacji:"))
        self.labelWynik1.setText(_translate("Form", ""))
        self.labelWynik2.setText(_translate("Form", ""))
        self.labelWynik3.setText(_translate("Form", ""))
        self.groupBox.setTitle(_translate("Form", ""))
        self.labelSekundy.setText(_translate("Form", "s"))
        self.labelZakres.setText("")
        self.inputFragment.setText('0')
        self.buttonPlay.setText(_translate("Form", "|>"))
        self.buttonStop.setText(_translate("Form", "| |"))

    def errorWindow(self, errMessage, details=""):
        error_msg = QMessageBox()
        error_msg.setIcon(QMessageBox.Critical)
        error_msg.setWindowTitle("Błąd!")
        error_msg.setText(errMessage)
        if details != "":
            error_msg.setDetailedText(details)
        error_msg.exec_()

    def generateLogMelSpec(self):
        # Parametry z GUI
        path = self.audiofile[0]
        start = int(self.timestamp)

        # Nasze parametry
        Fs = 22050
        img_size = 299
        n_mels = img_size  # number of bins in spectrogram. Height of image
        time_steps = img_size - 1  # number of time-steps. Width of image
        hop_length = int(self.window_time * Fs / time_steps)  # number of samples per time-step in spectrogram

        if self.duration - start > self.window_time:  # Fragment będzie trwał 30 s
            y, sr = librosa.load(path, mono=True, offset=start, duration=self.window_time)
            y_fs = librosa.resample(y, sr, Fs)
            wavfile.write('temp/audio.wav', sr, y_fs)

            S = librosa.feature.melspectrogram(y=y_fs,
                                               sr=Fs,
                                               n_mels=n_mels,
                                               hop_length=hop_length)

            S_dB = librosa.power_to_db(S, ref=np.max)

            png = np.flip(np.array(S_dB), axis=0)  # flip array so low frequencies are on the bottom
            scaler = preprocessing.MinMaxScaler()  # define scaler object for scaling
            png2 = scaler.fit_transform(png)  # scale data in matrix to values in range (0, 1)
            cmap = plt.get_cmap('inferno')  # optional parameter lut=N, N - resolution levels
            rgba_img = cmap(png2, bytes=False)  # create colormap in RGBA format
            logmelspec = np.delete(rgba_img, 3, 2)  # delete alpha channel

            fig_waveplot, ax_waveplot = plt.subplots()
            fig_waveplot.set_size_inches(9, 3)
            librosa.display.waveplot(y, sr=sr)
            ax_waveplot.set(title='Przebieg czasowy')
            ax_waveplot.set(xlabel='Czas [s]')
            ax_waveplot.set(ylabel='Amplituda')
            plt.savefig('temp/waveplot.png', dpi=100, bbox_inches='tight')

            self.graphicsWave.setPixmap(QtGui.QPixmap('temp/waveplot.png'))
            self.graphicsWave.setScaledContents(True)
            os.remove('temp/waveplot.png')

            fig_logmelspec, ax_logmelspec = plt.subplots()
            img = plt.imshow(png, cmap=cmap)
            fig_logmelspec.colorbar(img, ax=ax_logmelspec, format='%+2.0f dB')
            ax_logmelspec.set(title='Mel-spektrogram z amplitudą w skali logarytmicznej')
            ax_logmelspec.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: ('%g') % int((x / 299 * 30))))
            ax_logmelspec.yaxis.set_major_formatter(ticker.FuncFormatter(
                lambda x, pos: ('%g') % int(1127.0 * np.log(1.0 + (299 - x) * 11025 / 299 / 700.0))))
            ax_logmelspec.set(xlabel='Czas [s]')
            ax_logmelspec.set(ylabel='Mel')
            plt.savefig('temp/logmelspec.png', dpi=100)

            self.graphicsSpektrogram.setPixmap(QtGui.QPixmap('temp/logmelspec.png'))
            self.graphicsSpektrogram.setScaledContents(True)
            os.remove('temp/logmelspec.png')

        else:
            self.errorWindow(
                f"Wybrany fragment musi mieć 30 sekund! Maksymalna możliwa wartość do wpisania to {int(self.duration - self.window_time)} sekund.")
            return []

        return logmelspec

    def cnnPrediction(self, logmelspec):
        self.progressBar.setValue(30)
        self.progressBar.setVisible(True)
        prediction = self.model.predict(logmelspec[None, :, :, :])[0]
        self.progressBar.setValue(60)
        # ===== Clustering prediction =====
        cluster_size = len(prediction)
        clusters = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        clusters_number = 2

        while cluster_size > 3:
            clusters, _ = kmeans1d.cluster(prediction, clusters_number)
            cluster_value = clusters[list(prediction).index(max(list(prediction)))]
            cluster_size = clusters.count(cluster_value)
            clusters_number += 1

        self.progressBar.setValue(70)

        predicted_emotions = list(prediction * clusters)

        # ===== Return results =====
        CLASS_NAMES = ['Agresywny', 'Spokojny', 'Depresyjny',
                       'Energiczny', 'Ekscytujący', 'Wesoły',
                       'Relaksujący', 'Smutny', 'Straszny']

        result = np.array(predicted_emotions)
        indices = list(result.argsort()[-cluster_size:][::-1])

        self.progressBar.setValue(90)

        emotion_holder = []

        for i in indices:
            if predicted_emotions[i] != 0:
                emotion_holder.append(CLASS_NAMES[i])

        self.labelWynik1.setText(f"{emotion_holder[0]}: {round(prediction[indices[0]] * 100, 2)}%")
        if len(emotion_holder) >= 2:
            self.labelWynik2.setText(f"{emotion_holder[1]}: {round(prediction[indices[1]] * 100, 2)}%")
        else:
            self.labelWynik2.setText("")

        if len(emotion_holder) == 3:
            self.labelWynik3.setText(f"{emotion_holder[2]}: {round(prediction[indices[2]] * 100, 2)}%")
        else:
            self.labelWynik3.setText("")

        self.audiothread = multiprocessing.Process(target=playsound, args=("temp/audio.wav",))
        self.progressBar.setValue(100)
        self.progressBar.setVisible(False)
        self.buttonPlay.setDisabled(False)
        return

    def clickedWczytaj(self):
        try:
            self.audiothread.terminate()
            self.buttonPlay.setDisabled(True)
            self.buttonStop.setDisabled(True)
        except:
            pass
        self.audiofile = QFileDialog.getOpenFileName()
        if self.audiofile[0] == "":
            self.labelZakres.setText("")
            return
        elif self.audiofile[0][-3:].lower() != "wav":
            self.errorWindow("Niepoprawne rozszerzenie wczytywanego pliku.", "Program analizuje tylko pliki .wav!")
            self.audiofile = ("", "")
            self.labelZakres.setText("")
            return

        # Zczytaj informacje o pliku
        try:
            samplingFrequency, audioSamples = wavfile.read(self.audiofile[0])
            howManySamples = len(audioSamples)
        except:
            self.errorWindow("Nie można wczytać wybranego pliku audio. Może być on uszkodzony.")
            self.audiofile = ("", "")
            self.labelZakres.setText("")
            return

        self.duration = float(howManySamples) / float(samplingFrequency)

        if self.duration < self.window_time:
            self.errorWindow(
                f"Wczytany plik musi trwać minimum 30 sekund! Wybrany plik trwa {round(self.duration, 2)} sekund.")
            self.audiofile = ("", "")
            self.labelZakres.setText("")
            return

        self.labelZakres.setText(f"Wartości od 0 do {int(self.duration - self.window_time)} s")

    def clickedWyczysc(self):
        self.labelWynik1.setText("")
        self.labelWynik2.setText("")
        self.labelWynik3.setText("")
        self.labelZakres.setText("")
        self.inputFragment.setText("0")
        self.audiofile = ("", "")
        self.timestamp = 0
        self.duration = 0
        self.graphicsSpektrogram.clear()
        self.graphicsWave.clear()
        if os.path.isfile('temp/audio.wav'):
            os.remove('temp/audio.wav')
        try:
            self.audiothread.terminate()
            self.buttonPlay.setDisabled(True)
            self.buttonStop.setDisabled(True)
        except:
            pass

    def clickedAnalizuj(self):
        try:
            self.audiothread.terminate()
            self.buttonPlay.setDisabled(True)
            self.buttonStop.setDisabled(True)
        except:
            pass
        error_message = ""
        self.timestamp = self.inputFragment.text()
        if self.timestamp == "":
            error_message += "Nie podano początku 30-sekundowego fragmentu. "
        elif int(self.timestamp) < 0:
            error_message += "Początek 30-sekundowego fragmentu musi być wartością większą lub równą 0. "
        if self.audiofile[0] == "":
            error_message += "Nie wczytano żadnego pliku audio. "

        if error_message != "":
            self.errorWindow(error_message)
            return

        self.labelWynik1.setVisible(True)
        self.labelWynik2.setVisible(True)
        self.labelWynik3.setVisible(True)

        logmelspec = self.generateLogMelSpec()

        if len(logmelspec) == 0:
            return
        self.cnnPrediction(logmelspec)

    def clickedPlay(self):
        try:
            self.audiothread.start()
            self.buttonPlay.setDisabled(True)
            self.buttonStop.setDisabled(False)
        except:
            return

    def clickedStop(self):
        try:
            self.audiothread.terminate()
            self.buttonStop.setDisabled(True)
        except:
            return

    def closeEvent(self, event):
        try:
            self.audiothread.terminate()
            while self.audiothread.is_alive():
                continue
        except:
            pass
        if os.path.isfile('temp/audio.wav'):
            os.remove('temp/audio.wav')
        if os.path.isdir('temp'):
            if len(os.listdir('temp')) == 0:
                os.rmdir('temp')


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Form = Ui_Form()
    Form.setupUi()
    Form.show()
    sys.exit(app.exec())
